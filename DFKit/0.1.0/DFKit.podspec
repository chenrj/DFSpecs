Pod::Spec.new do |s|
  s.name             = 'DFKit'
  s.version          = '0.1.0'
  s.summary          = 'A collection of iOS components for DFRZW.'
  s.homepage         = 'https://gitlab.com/chenrj/DFKit'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'chenrj90' => 'chenrj90@163.com' }
  s.source           = { :git => 'https://chenrj@gitlab.com/chenrj/DFKit.git', :tag => '0.1.0' }
  s.ios.deployment_target = '8.0'
  s.source_files = 'DFKit/Classes/**/*'
  # s.resource_bundles = {
  #   'DFKit' => ['DFKit/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
