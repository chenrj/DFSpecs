Pod::Spec.new do |s|
    s.name             = 'DFKit'
    s.version          = '0.1.1'
    s.summary          = 'A collection of iOS components for DFRZW.'
    s.homepage         = 'https://gitlab.com/chenrj/DFKit'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'chenrj90' => 'chenrj90@163.com' }
    s.source           = { :git => 'https://chenrj@gitlab.com/chenrj/DFKit.git', :tag => '0.1.1' }
    s.ios.deployment_target = '8.0'
    s.source_files = 'DFKit/Classes/**/*'
    # s.resource_bundles = {
    #   'DFKit' => ['DFKit/Assets/*.png']
    # }

    # s.public_header_files = 'Pod/Classes/**/*.h'
    # s.frameworks = 'UIKit', 'MapKit'
    s.dependency 'Masonry', '~> 1.0.2'
    s.dependency 'Aspects', '~> 1.4.1'
    s.dependency 'FDFullscreenPopGesture', '~> 1.1'

    s.subspec 'Share' do |ss|
        ss.source_files =   'DFKit/Share/**/*'
        ss.library = 'sqlite3'
        ss.ios.vendored_library = 'DFKit/Frameworks/Share/**/*.a'
        ss.ios.vendored_frameworks = 'DFKit/Frameworks/Share/**/*.framework'
    end

    s.subspec 'Analyze' do |ss|
        ss.source_files =   'DFKit/Analyze/**/*'
        ss.library = 'z', 'sqlite3'
        ss.frameworks = 'CoreTelephony'
        ss.ios.vendored_library = 'DFKit/Frameworks/Analyze/**/*.a'
        ss.ios.vendored_frameworks = 'DFKit/Frameworks/Analyze/**/*.framework'
    end

end
